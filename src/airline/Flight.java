/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package airline;

import java.util.Date;

/**
 *
 * @author DHAIRYA
 */
public class Flight {
    
    private String code;
    private Date date;
    
    public Flight(String code, Date date)
    {
        this.code = code;
        this.date = date;
        
    }

    public String getCode() {
        return this.code;
    }

    public Date getDate() {
        return this.date;
    }
    
    
}
