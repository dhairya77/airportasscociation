/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package airline;

/**
 *
 * @author DHAIRYA
 */
public class Airline {
    private String name;
    private String short_code;

    public Airline(String name, String short_code) {
        this.name = name;
        this.short_code = short_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_code() {
        return short_code;
    }

    public void setShort_code(String short_code) {
        this.short_code = short_code;
    }
    
    
}
