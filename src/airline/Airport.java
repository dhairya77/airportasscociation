/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package airline;

/**
 *
 * @author DHAIRYA
 */
public class Airport {
    private String code;
    private String city;

    public Airport(String code, String city) {
        this.code = code;
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }
    
    
}
