/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package airline;

/**
 *
 * @author DHAIRYA
 */
public class Airplane {
    private String model;
    private String unique_id;
    private String seats;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public Airplane(String model, String unique_id, String seats) {
        this.model = model;
        this.unique_id = unique_id;
        this.seats = seats;
    }
    
    
}
